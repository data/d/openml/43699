# OpenML dataset: QSAR-Bioconcentration-Classes-Data-Set

https://www.openml.org/d/43699

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Data Set Information:
A dataset of manually-curated BCF for 779 chemicals was used to determine the mechanisms of bioconcentration, i.e. to predict whether a chemical: (1) is mainly stored within lipid tissues, (2) has additional storage sites (e.g. proteins), or (3) is metabolized/eliminated. Data were randomly split into a training set of 584 compounds (75) and a test set of 195 compounds (25), preserving the proportion between the classes. Two QSAR classification trees were developed using CART (Classification and Regression Trees) machine learning technique coupled with Genetic Algorithms. The file contains the selected Dragon descriptors (9) along with CAS, SMILES, experimental BCF, experimental/predicted KOW and mechanistic class (1, 2, 3). Further details on model development and performance along with descriptor definitions and interpretation are provided in the original manuscript (Grisoni et al., 2016).
Attribute Information:
3 Compound identifiers:

CAS number
Molecular SMILES
Train/test splitting

9 molecular descriptors (independent variables)

nHM
piPC09
PCD
X2Av
MLOGP
ON1V
N-072
B02[C-N]
F04[C-O]

2 experimental responses:

Bioconcentration Factor (BCF) in log units (regression)
Bioaccumulation class (three classes)

Relevant Papers:
F. Grisoni, V.Consonni, M.Vighi, S.Villa, R.Todeschini (2016). Investigating the mechanisms of bioconcentration through QSAR classification trees, Environment International, 88, 198-205

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43699) of an [OpenML dataset](https://www.openml.org/d/43699). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43699/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43699/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43699/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

